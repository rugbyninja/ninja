<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MatchDeclinedClub extends Mailable
{
    use Queueable, SerializesModels;

    public $record;
    public $store;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($record, $store)
    {
        $this->record = $record;
        $this->store = $store;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.match-declined-club')->subject('Match Made!')
                                                        ->with('record', $this->record)
                                                        ->with('store', $this->store);
    }
}
