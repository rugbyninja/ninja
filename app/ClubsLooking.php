<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\Google;
use DB;

class ClubsLooking extends Model
{
    protected $table = 'clubs_looking';

    protected $fillable = [
    	'club_id', 'opposition', 'date', 'time', 'postcode', 'latitude', 'longitude', 'forwards', 'backs', 'tighthead', 'loosehead', 'hooker', 'jumper', 'lifter', 'kicker', 'players_needed',
    ];

    /**
     * Returns the update form to the user
     * @return type
     */
    public static function updateForm()
    {
    	$latest = ClubsLooking::where('club_id', Auth::guard('club')->user()->id)->whereDate('created_at', '>=', date('Y-m-d', strtotime('last sunday')))->orderBy('created_at', 'desc')->first();

    	if(! $latest){
    		return view('club.partials.looking-form-create');
    	}
		
		return view('club.partials.looking-form-update')->with('latest', $latest);
    }

    /**
     * Returns 4 random currently available players
     * @return type
     */
    public static function randomLooking()
    {
        return ClubsLooking::select('a.*')
            ->from(DB::raw('(SELECT *, (3959 * acos( cos( radians('.Auth::user()->latitude.') ) *
                               cos( radians( latitude ) )
                               * cos( radians( longitude ) - radians('.Auth::user()->longitude.')
                               ) + sin( radians('.Auth::user()->latitude.') ) *
                               sin( radians( latitude ) ) )
                             ) AS distance FROM clubs_looking ORDER BY created_at DESC) a'))
            ->where('date', '>=', date('Y-m-d'))
            ->limit(4)
            ->get();
    }

    /**
     * Performs a search for available players, filtered on position and postcode radus (defaults to 25 miles of clubs location if nothing set)
     * @return type
     */
    public static function search()
    {
        if(! empty(Request()->position) && ! empty(Request()->postcode) && ! empty(Request()->distance)){

            $google = new Google;
            $latLng = $google->latLng(Request()->postcode);

            return ClubsLooking::select('clubs_looking.*')
                ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                                   cos( radians( latitude ) )
                                   * cos( radians( longitude ) - radians(?)
                                   ) + sin( radians(?) ) *
                                   sin( radians( latitude ) ) )
                                 ) AS distance', [$latLng->lat, $latLng->lng, $latLng->lat])
                ->havingRaw("distance < ?", [Request()->distance])
                ->where(Request()->position, 1)
            	->where('date', '>=', date('Y-m-d'))
                ->orderBy('distance', 'asc')
                ->simplePaginate(10);
        }

        return ClubsLooking::select('clubs_looking.*')
            ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                               cos( radians( latitude ) )
                               * cos( radians( longitude ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( latitude ) ) )
                             ) AS distance', [Auth::user()->latitude, Auth::user()->longitude, Auth::user()->latitude])
            ->havingRaw("distance < ?", [25])
            ->where('date', '>=', date('Y-m-d'))
            ->orderBy('distance', 'asc')
            ->simplePaginate(10);
    }

    public static function latestClubRequest()
    {
    	$latest = ClubsLooking::where('club_id', Auth::guard('club')->user()->id)->whereDate('created_at', '>=', date('Y-m-d', strtotime('last sunday')))->first();

		if($latest){
			return $latest->id;
		}

		return 0;
    }
}