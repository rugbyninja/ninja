<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Club extends Authenticatable
{
    protected $guard = 'clubs';

    protected $fillable = [
        'name', 'email', 'club', 'first_address', 'town', 'county', 'postcode', 'county_board', 'level', 'password', 'latitude', 'longitude',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Returns a record based on the given id
     * @param int $id 
     * @return type
     */
    public static function id(int $id)
    {
    	return Club::where('id', $id)->first();
    }
}
