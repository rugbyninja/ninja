<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Http\Services\Google;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'club', 'position', 'tighthead', 'loosehead', 'hooker', 'jumper', 'lifter', 'kicker', 'postcode', 'latitude', 'longitude',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Returns a users record from a given id
     * @param type $id 
     * @return type
     */
    public static function id($id)
    {
        return User::where('id', $id)->first();
    }

    /**
     * Returns 4 random currently available players
     * @return type
     */
    public static function randomAvailable()
    {
        return User::select('users.*')
            ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                               cos( radians( latitude ) )
                               * cos( radians( longitude ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( latitude ) ) )
                             ) AS distance', [Auth::guard('club')->user()->latitude, Auth::guard('club')->user()->longitude, Auth::guard('club')->user()->latitude])
            ->inRandomOrder()
            ->limit(4)
            ->get();
    }

    /**
     * Performs a search for available players, filtered on position and postcode radus (defaults to 25 miles of clubs location if nothing set)
     * @return type
     */
    public static function search()
    {   
        if(! empty(Request()->position) && ! empty(Request()->postcode) && ! empty(Request()->distance)){

            $google = new Google;
            $latLng = $google->latLng(Request()->postcode);

            return User::select('users.*')
                ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                                   cos( radians( latitude ) )
                                   * cos( radians( longitude ) - radians(?)
                                   ) + sin( radians(?) ) *
                                   sin( radians( latitude ) ) )
                                 ) AS distance', [$latLng->lat, $latLng->lng, $latLng->lat])
                ->havingRaw("distance < ?", [Request()->distance])
                ->where('position', Request()->position)
                ->orderBy('distance', 'asc')
                ->simplePaginate(10);
        }

        return User::select('users.*')
            ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                               cos( radians( latitude ) )
                               * cos( radians( longitude ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( latitude ) ) )
                             ) AS distance', [Auth::guard('club')->user()->latitude, Auth::guard('club')->user()->longitude, Auth::guard('club')->user()->latitude])
            ->havingRaw("distance < ?", [25])
            ->orderBy('distance', 'asc')
            ->simplePaginate(10);
    }

    /**
     * Returns an array of all position options
     * @return type
     */
    public static function positions()
    {
        return [
            'forwards' => 'Forwards',
            'backs' => 'Backs',
            'all-round' => 'All Round',
        ];
    }

    /**
     * Returns an array of all distances available as a filter option
     * @return type
     */
    public static function distance()
    {
        return [
            5 => '5 Miles',
            10 => '10 Miles',
            15 => '15 Miles',
            20 => '20 Miles',
            25 => '25 Miles',
            1000000 => '25+ Miles',
        ];
    }
}