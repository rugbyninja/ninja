<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Match extends Model
{
    public $fillable = [
    	'player_id', 'club_id', 'club_looking_id', 'originator', 'status', 'notes',
    ];

    public static function getStatuses()
    {
    	return ['Pending', 'Accepted', 'Declined'];
    }

    /**
     * Shows all historical requests for a logged in player
     * @return type
     */
    public static function playerRequests()
    {
    	return Match::select('matches.*', 'clubs_looking.opposition', 'clubs_looking.date', 'clubs_looking.time', 'clubs_looking.postcode')->join('clubs_looking', 'club_looking_id', '=', 'clubs_looking.id')->where('player_id', Auth::user()->id)->paginate(12);
    }

    /**
     * Shows all historical requests for a logged in club
     * @return type
     */
    public static function clubRequests()
    {
    	return Match::select('matches.*', 'clubs_looking.opposition', 'clubs_looking.date', 'clubs_looking.time', 'clubs_looking.postcode')->join('clubs_looking', 'club_looking_id', '=', 'clubs_looking.id')->where('player_id', Auth::guard('club')->user()->id)->paginate(12);
    }

    /**
     * Checks if the logged in player has already submitted or received a given request
     * @param type $id 
     * @return type
     */
    public static function playerCheck($clubId, $clubLookingId)
    {
    	try{
    		$request = Match::where([['player_id', Auth::user()->id],['club_id', $clubId],['club_looking_id', $clubLookingId]])->first();

    		if($request){
    			return true;
    		}

    		return false;
    	} catch(\Exception $e){
    		return false;
    	}
    }

    /**
     * Checks if the logged in player has already submitted or received a given request
     * @param type $id 
     * @return type
     */
    public static function clubCheck($playerId)
    {
    	try{
    		$request = Match::where([['club_id', Auth::guard('club')->user()->id],['player_id', $playerId],['club_looking_id', ClubsLooking::latestClubRequest()]])->first();

    		if($request){
    			return true;
    		}

    		return false;
    	} catch(\Exception $e){
    		return false;
    	}
    }
}
