<?php

namespace App\Http\Services;

class Google
{
	/**
	 * @var Api key for requests to Google API platform
	 */
    public $apiKey = '&key=AIzaSyBdTshNSHfCePfUULyi19QGQYdZW5Ycazw';

    /**
     * @var Geolocation url used in requests
     */
    public $geoLocationUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';

    /**
     * Uses the google geocode api to get the latitude and longitude of a given address
     * @param string $address 
     * @return type
     */
    public function latLng(string $address)
    {
        try{
            $address = urlencode($address);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->geoLocationUrl.'address='.$address.$this->apiKey);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch); 

            $output = json_decode($output);

            return $output->results[0]->geometry->location;
        } catch(\Exception $e){
            $return = new \stdClass();
            $return->lat = 53.251399;
            $return->lng = -1.816980;

            return $return;
        }

    }
}
