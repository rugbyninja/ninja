<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Services\Google;
use App\Mail\NewClub;
use App\Club;

class ClubController extends Controller
{
	/**
	 * Registers a new club
	 * @param Request $request 
	 * @return type
	 */
    public function register(Request $request)
    {
    	$create = $request->except('_token', 'password_confirmation');

    	if($create['password']!==$request->password_confirmation){
    		return redirect()->back()->with('danger', 'Please make sure the password confirmation matches');
    	}

    	$create['password'] = Hash::make($create['password']);
    	
        $google = new Google;
        $latLng = $google->latLng($create['postcode']);

        $create['latitude'] = $latLng->lat;
        $create['longitude'] = $latLng->lng;

    	Club::create($create);

    	if (Auth::guard('club')->attempt(['email' => $create['email'], 'password' => $request->password])) {
            Mail::to($create['email'])->send(new NewClub($create));
    		return redirect('/club-account');
		}

		return redirect()->back()->with('danger', 'Sorry, there was an error in creating your account, please try again and contact us if the error persists');
    }

    /**
     * Logs in a club
     * @param Request $request 
     * @return type
     */
    public function login(Request $request)
	{
		if (Auth::guard('club')->attempt(['email' => $request->email, 'password' => $request->password])) {
			return redirect('/club-account');
		}

		return redirect()->back()->with('danger', 'Those credentials were not recognised');

    }

    public function index()
    {
    	return view('club.view')->with('view', 'dashboard');
    }

    public function requests()
    {
        return view('club.view')->with('view', 'my-requests');
    }
}
