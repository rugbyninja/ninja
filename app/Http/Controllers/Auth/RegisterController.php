<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Services\Google;
use App\Mail\NewPlayer;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'club' => ['required', 'string'],
            'postcode' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if(! isset($data['club'])){
            $data['club'] = 'No Club';
        }
        if(! isset($data['tighthead'])){
            $data['tighthead'] = 0;
        }
        if(! isset($data['loosehead'])){
            $data['loosehead'] = 0;
        }
        if(! isset($data['hooker'])){
            $data['hooker'] = 0;
        }
        if(! isset($data['jumper'])){
            $data['jumper'] = 0;
        }
        if(! isset($data['lifter'])){
            $data['lifter'] = 0;
        }
        if(! isset($data['kicker'])){
            $data['kicker'] = 0;
        }

        $create = [];
        $create['name'] = $data['name'];
        $create['email'] = $data['email'];
        $create['password'] = Hash::make($data['password']);
        $create['club'] = $data['club'];
        $create['position'] = $data['position'];
        $create['tighthead'] = $data['tighthead'];
        $create['loosehead'] = $data['loosehead'];
        $create['hooker'] = $data['hooker'];
        $create['jumper'] = $data['jumper'];
        $create['lifter'] = $data['lifter'];
        $create['kicker'] = $data['kicker'];
        $create['postcode'] = $data['postcode'];

        $google = new Google;
        $latLng = $google->latLng($create['postcode']);

        $create['latitude'] = $latLng->lat;
        $create['longitude'] = $latLng->lng;

        Mail::to($create['email'])->send(new NewPlayer($create));

        return User::create($create);
    }
}
