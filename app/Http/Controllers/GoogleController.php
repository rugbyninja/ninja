<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GoogleController extends Controller
{
	/**
	 * @var Api key for requests to Google API platform
	 */
    public $apiKey = '&key=AIzaSyBdTshNSHfCePfUULyi19QGQYdZW5Ycazw';

    /**
     * @var Geolocation url used in requests
     */
    public $geoLocationUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';

    /**
     * Uses the google geocode api to get the latitude and longitude of a given address
     * @param string $address 
     * @return type
     */
    public function latLng(string $address)
    {
    	$address = urlencode($address);
 		$url = $this->geoLocationUrl.$address.$apiKey;
 		dd($url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->geoLocationUrl.$address.$apiKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch); 

		dd($output);

    }
}
