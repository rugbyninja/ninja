<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function view($slug)
    {
    	return view('view')->with('view', $slug);
    }

    public function index()
    {
    	return $this->view('index');
    }

    public function players()
    {
    	return $this->view('players');
    }
}
