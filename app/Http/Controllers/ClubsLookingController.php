<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\Google;
use App\ClubsLooking;

class ClubsLookingController extends Controller
{
    public function store(Request $request)
    {
    	try {
    		$store = $request->except('_token');
    		$store['club_id'] = Auth::guard('club')->user()->id;
    	
	        $google = new Google;
	        $latLng = $google->latLng($store['postcode']);

	        $store['latitude'] = $latLng->lat;
	        $store['longitude'] = $latLng->lng;

    		ClubsLooking::updateOrCreate(['id' => $request->id, 'club_id' => Auth::guard('club')->user()->id], $store);

    		return redirect()->back()->with('success', 'You\'ve successfully updated your clubs requirements');

    	} catch(\Exception $e){
    		return redirect()->back()->with('danger', 'Sorry, we were unable to update your clubs requirements. Please try again and contact us if this error persists'.$e);
    	}
    }
}
