<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MatchAcceptedClub;
use App\Mail\MatchAcceptedPlayer;
use App\Mail\MatchDeclinedClub;
use App\Mail\MatchDeclinedPlayer;
use App\Club;
use App\Match;
use App\User;

class MatchController extends Controller
{
	/**
	 * Updates or creates a request from a club to a player
	 * @param Request $request 
	 * @return type
	 */
    public function clubRequest(Request $request)
    {
    	try{
	    	$store = $request->except('_token');
	    	$store['club_id'] = Auth::guard('club')->user()->id;

	    	$record = Match::updateOrCreate(['id' => $request->id, 'club_id' => $store['club_id']], $store);

	    	if($record->wasRecentlyCreated){
	    	return redirect()->back()->with('success', 'Your request was successfully sent! We\'ve contacted the player to let them know and will confirm once we\'ve had a response from them');
	    	}
			
	    	if(isset($store['status'])){
	    		$this->statusEmail($record, $store);
	    	}

	    	return redirect()->back()->with('success', 'We\'ve updated this request for you and have updated the player');
    	} catch(\Exception $e){
    		return redirect()->back()->with('danger', 'Sorry, we were unable to carry out your request. Please try again and contact us if this error persists');
    	}
    }

	/**
	 * Updates or creates a request from a player to a club
	 * @param Request $request 
	 * @return type
	 */
    public function playerRequest(Request $request)
    {
    	try{
	    	$store = $request->except('_token');
	    	$store['player_id'] = Auth::user()->id;

	    	$record = Match::updateOrCreate(['id' => $request->id, 'player_id' => $store['player_id']], $store);

	    	if($record->wasRecentlyCreated){
	    		return redirect()->back()->with('success', 'Your request was successfully sent! We\'ve contacted the club to let them know and will confirm once we\'ve had a response from them');
	    	}
			
	    	if(isset($store['status'])){
	    		$this->statusEmail($record, $store);
	    	}

	    	return redirect()->back()->with('success', 'We\'ve updated this request for you and have updated the club');
    	} catch(\Exception $e){
    		return redirect()->back()->with('danger', 'Sorry, we were unable to carry out your request. Please try again and contact us if this error persists');
    	}
    }

    /**
     * Sends a new email when the status ahs been updated
     * @param type $record 
     * @param type $store 
     * @return type
     */
    public function statusEmail($record, $store)
    {
    	try{
	    	if($store['status']=='Accepted'){
	    		Mail::to(Club::id($record['club_id']))->send(new MatchAcceptedClub($record, $store));
	    		Mail::to(User::id($record['player_id']))->send(new MatchAcceptedPlayer($record, $store));
	    	}

	    	if($store['status']=='Declined'){
	    		Mail::to(Club::id($record['club_id']))->send(new MatchDeclinedClub($record, $store));
	    		Mail::to(User::id($record['player_id']))->send(new MatchDeclinedPlayer($record, $store));
	    	}

	    	return true;
    	} catch(\Exception $e){
    		return false;
    	}
    }
}
