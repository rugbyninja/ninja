<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends Controller
{
	/**
	 * Returns the account dashboard to a user
	 * @return type
	 */
    public function index()
    {
    	return view('account.view')->with('view', 'dashboard');
    }

    /**
     * Returns the update form
     * @return type
     */
    public function update()
    {
    	return view('account.view')->with('view', 'update');
    }

    /**
     * Shows a user all their requests
     * @return type
     */
    public function requests()
    {
        return view('account.view')->with('view', 'my-requests');
    }
}
