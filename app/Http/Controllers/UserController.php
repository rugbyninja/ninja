<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
	/**
	 * Updates a users details based on form submitted data
	 * @return type
	 */
    public function store(Request $request)
    {
    	try{
            $store = $request->except('_token');

            if(! isset($store['tighthead'])){
                $store['tighthead'] = 0;
            }

            if(! isset($store['loosehead'])){
                $store['loosehead'] = 0;
            }

            if(! isset($store['hooker'])){
                $store['hooker'] = 0;
            }

            if(! isset($store['jumper'])){
                $store['jumper'] = 0;
            }

            if(! isset($store['lifter'])){
                $store['lifter'] = 0;
            }

            if(! isset($store['kicker'])){
                $store['kicker'] = 0;
            }

    		User::where('id', Auth::user()->id)->update($store);
    		return redirect()->back()->with('success', 'We have successfully updated your details');
    	}
    	catch(\QueryException $e){
    		return redirect()->back()->with('danger', 'Sorry we could not update your details, please try again or contact us if the problem persists');
    	}
    }
    /**
     * Updates a users details based on form submitted data
     * @return type
     */
    public function availability(Request $request)
    {
        try{
            User::where('id', Auth::user()->id)->update($request->except('_token'));
            return redirect()->back()->with('success', 'We have successfully updated your details');
        }
        catch(\QueryException $e){
            return redirect()->back()->with('danger', 'Sorry we could not update your details, please try again or contact us if the problem persists');
        }
    }
}
