<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/players', 'PageController@players')->name('players');
Route::get('/logout', 'Auth\LoginController@logout');

Route::post('/club/register', 'ClubController@register');
Route::post('/club/login', 'ClubController@login');

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function(){
	Route::post('availability', 'UserController@availability');
});

Route::group(['prefix' => 'account', 'middleware' => 'auth'], function(){
	Route::get('/', 'AccountController@index');
	Route::get('my-details', 'AccountController@update');
	Route::post('my-details', 'UserController@store');
	Route::get('my-requests', 'AccountController@requests');
	Route::post('request', 'MatchController@playerRequest');
});

Route::group(['prefix' => 'club-account', 'middleware' => 'club'], function(){
	Route::get('/', 'ClubController@index');
	Route::get('my-requests', 'ClubController@requests');
	Route::post('availability', 'ClubsLookingController@store');
	Route::post('request', 'MatchController@clubRequest');
});

Route::get('/', 'PageController@index');
Route::get('{slug}', 'PageController@view');
