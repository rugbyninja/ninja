@include('partials.header')
@include('partials.navigation')
@include('partials.alerts')
@yield('content')

@include('partials.footer')