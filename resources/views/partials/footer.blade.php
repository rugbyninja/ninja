	<footer class="footer mt-auto py-3">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-12 quicklinks">
					<h4 class="h4">Quicklinks</h4>
					
						<a href="">Home</a> |
						<a href="">Player</a> |
						<a href="">Club</a> |
						<a href="">Terms and Conditions</a> |
						<a href="">Privacy Policy</a> 
	 			</div>
				<div class="col-md-6 col-12">
					<p class="text-muted float-right">Rugby Ninja - &copy; 2018</p>
				</div>
			</div>
		</div>
	</footer>

	<script>
	
	</script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"> </script>
    <script src="/js/custom.js"></script>
  </body>
</html>