<nav class="navbar navbar-expand-lg navbar-light h-nav">
   <a class="logo mt-4" href="/"><img src="/images/logo.png"></a>
  <button class="navbar-toggler nav-position" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="burger"></span>
    <span class="burger"></span>
    <span class="burger"></span>  
  </button>

  <div class="collapse navbar-collapse float-right text-right" id="navbarTogglerDemo02">
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
      @if(Auth::check())
      <li class="nav-item">
        <a class="nav-link" href="/account">{{ Auth::user()->name }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/account/my-details">My Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/account/my-requests">My Requests</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/logout">Logout</a>
      </li>
      @elseif(Auth::guard('club')->check())
      <li class="nav-item">
        <a class="nav-link" href="/club-account">{{ Auth::guard('club')->user()->club }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/club-account/my-requests">My Requests</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/logout">Logout</a>
      </li>
      @else
      <li class="nav-item">
        <a class="nav-link" href="/players">Players</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/clubs">Clubs</a>
      </li>
      @endif
    </ul>
  </div>
</nav>
 