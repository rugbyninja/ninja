@if(Session::has('danger'))
<div class="alert alert-danger">
     <a class="close" data-dismiss="alert" style="text-decoration: none;" aria-label="close">×</a>
    {!! Session::get('danger') !!}
</div>
@endif
  
@if(Session::has('info'))
<div class="alert alert-info">
     <a class="close" data-dismiss="alert" style="text-decoration: none;" aria-label="close">×</a>
    {!! Session::get('info') !!}
</div>
@endif
  
@if(Session::has('success'))
<div class="alert alert-success">
     <a class="close" data-dismiss="alert" style="text-decoration: none;" aria-label="close">×</a>
    {!! Session::get('success') !!}
</div>
@endif
  
@if(Session::has('warning'))
<div class="alert alert-warning">
     <a class="close" data-dismiss="alert" style="text-decoration: none;" aria-label="close">×</a>
    {!! Session::get('warning') !!}
</div>
@endif