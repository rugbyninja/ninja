<div class="container-fluid mt-hero">
  <div class="row h-100">
    <div class="col-md-12 p-0 text-center h-100 overflow">
      <div class="d-block hero-player-img" style="background-image: url('/images/register-player.jpg'); background-size: cover; background-position: center center;" alt="First slide">
      	<div class="d-block curve" style="background-image: url('/images/curve.svg'); background-size: cover; background-position: center center;" alt="First slide"></div>
      </div>
        <div class="carousel-caption">
          <h2>Register or log in as a club.</h2>
         </div>
    </div>
  </div>
</div>

<div class="container py-3">
	<div class="row">
		<div class="col-md-4 mb-5">
			<h3 class="h3">Clubs Login</h3>
			<form action="/club/login" method="post">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Email:</label>
					<input type="email" name="email" class="form-control" placeholder="Enter your email" required>
				</div>
				<div class="form-group">
					<label>Password:</label>
					<input type="password" name="password" class="form-control" placeholder="Enter your password" required>
				</div>
				<button typ="submit" class="btn-secondary">Login <i class="fas fa-long-arrow-alt-right"></i></button>
			</form>
		</div>
		<div class="col-md-8">
			<h3 class="h3">Club Registration</h3>
			<form action="/club/register" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Your Name:</label>
							<input type="name" name="name" class="form-control" placeholder="Enter your name" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Email:</label>
							<input type="email" name="email" class="form-control" placeholder="Enter your email" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Club Name and Level: (i.e. Test Club 2nd XV)</label>
							<input type="text" name="club" class="form-control" placeholder="Enter your club name" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>County Board</label>
							<select name="county_board" required>
								<option value="" disabled selected>Please choose an option...</option>
								<option value="forwards">North Midlands</option>
								<option value="backs">Staffordshire</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>League</label>
							<select name="level" required>
								<option value="" disabled selected>Please choose an option...</option>
								<option value="Midlands 5">Midlands 5</option>
								<option value="Merit">Merit League</option>
								<option value="Other">Other</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>1st Line of Address:</label>
							<input type="text" name="first_address" class="form-control" placeholder="Enter your 1st Line of Address"required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Town:</label>
							<input type="text" name="town" class="form-control" placeholder="Enter your town"required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>County:</label>
							<input type="text" name="county" class="form-control" placeholder="Enter your county"required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Postcode:</label>
							<input type="text" name="postcode" class="form-control" placeholder="Enter your postcode"required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Password:</label>
							<input type="password" name="password" class="form-control" placeholder="Create a password"required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Confrim Password:</label>
							<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm your password"required>
						</div>
					</div>
				</div>
				<button typ="submit" class="btn-secondary">Register <i class="fas fa-long-arrow-alt-right"></i></button>
			</form>
		</div>
	</div>
</div>
