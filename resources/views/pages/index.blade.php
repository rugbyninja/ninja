<div class="container-fluid mt-hero">
    <div class="row h-100">
        <div class="col-md-12 p-0 text-center h-100 overflow">
            <div class="d-block hero-img" style="background-image: url('/images/rugby-club.jpg'); background-size: cover; background-position: center center;" alt="First slide">
                <div class="curve" style="background-image: url('/images/curve.svg'); background-size: cover; background-position: center bottom;">
                </div>
                
                <div class="carousel-caption d-none d-md-block">
                    <h2>Because Saturday's are for Rugby</h2>
                    <h3 class="h3-subheader">The best way to find a game on your weekends</h3>
                    <div class="row align-items-center justify-content-center">
                        <div class="col-12 pt-4">
                            <a href="#" class="btn-primary m-2">Register as a player <i class="fas fa-long-arrow-alt-right p-1"></i></a>
                            <a href="#" class="btn-primary m-2">Register as a club <i class="fas fa-long-arrow-alt-right p-1"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container py-3">
         <div class="card-deck">
             <div class="card p-0">
                <div class="d-block card-img" style="background-image: url('/images/rugby-player.jpg'); background-size: cover; position: relative; background-position: center center;" alt="First slide"></div>
                <div class="card-body card-body-before">
                    <h5 class="card-title">Player Match</h5>
                    <p class="card-text">For players who find themselves stuck without a game and in need of a club</p>
                    <a href="/players" class="btn-secondary">Go somewhere</a>
                </div>
            </div>
 
             <div class="card p-0">
                <div class="d-block card-img" style="background-image: url('/images/card-club.jpg'); background-size: cover; position: relative; background-position: center center;" alt="First slide"></div>
                <div class="card-body card-body-before">
                    <h5 class="card-title">Club Match</h5>
                    <p class="card-text">For clubs who find themselves in need of a single or group of players</p>
                    <a href="/clubs" class="btn-secondary">Go somewhere</a>
                </div>
            </div>

            <div class="card p-0">
                <div class="d-block card-img" style="background-image: url('/images/card-club.jpg'); background-size: cover; position: relative; background-position: center center;" alt="First slide"></div>
                <div class="card-body card-body-before">
                    <h5 class="card-title">Team Match</h5>
                    <p class="card-text">For a group of players wanting to travel together to help another club</p>
                    <a href="/teams" class="btn-secondary">Go somewhere</a>
                </div>
            </div>
        </div>
    </div>
 