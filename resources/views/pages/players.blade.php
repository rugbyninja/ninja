<div class="container-fluid mt-hero">
  <div class="row h-100">
    <div class="col-md-12 p-0 text-center h-100 overflow">
      <div class="d-block hero-player-img" style="background-image: url('/images/register-player.jpg'); background-size: cover; background-position: center center;" alt="First slide">
      	    <div class="d-block curve" style="background-image: url('/images/curve.svg'); background-size: cover; background-position: center center;" alt="First slide"></div>
      </div>
        <div class="carousel-caption">
          <h2>Register as a player</h2>
         </div>
    </div>
  </div>
</div>
<div class="container py-3">
	<div class="row">
		<div class="col-md-4">
			<h3 class="h3">Player Login</h3>
			<form action="/login" method="post">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Email:</label>
					<input type="email" name="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Password:</label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<button typ="submit" class="btn-secondary">Login <i class="fas fa-long-arrow-alt-right"></i></button>
			</form>
		</div>
		<div class="col-md-8">
			<h3 class="h3">Player Registration</h3>
			<form action="/register" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-12">
						<div class="row justify-content-center">
							<div class="col-md-6">
								<div class="form-group">
									<label>Name:</label>
									<input type="name" name="name" class="form-control" placeholder="Enter your Name" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Email:</label>
									<input type="email" name="email" class="form-control" placeholder="Enter your Email Address" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Postcode:</label>
									<input type="text" name="postcode" class="form-control" placeholder="Enter your Postcode" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Club Name:</label>
									<input type="text" name="club" class="form-control" placeholder="Enter your club (optional)">
								</div>
							</div>
							<div class="col-md-2 mt-3">
								<ul class="phq-checkboxes pt-checkboxes">
				                    <li class="d-inline pr-1">
									    <input type="checkbox" name="club" value"No Club" class="form-check-input" id="noClub">
				                    	<label class="mt-2" for="noClub">No Club</label>
				                    </li>
  				                </ul>
  				             </div>
	
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>What's your usual position?</label>
							<select name="position" class="select" required>
								<option value="" disabled selected>Please choose an option...</option>
								<option value="forwards">Forwards</option>
								<option value="backs">Backs</option>
								<option value="all-round">Put me anywhere!</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group mb-0">
							<label>Can you play any of these specialist positions? (select all that apply)</label>
							<div class="row">
								<div class="col-md-10">
									<ul class="phq-checkboxes pt-checkboxes mb-2">
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="tighthead" value"1" class="form-check-input" id="tighthead">
 					                   		<label for="tighthead">Tighthead Prop</label>
 					                   </li>
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="loosehead" value"1" class="form-check-input" id="loosehead">
					                   		<label for="loosehead">Loosehead Prop</label>
					                   </li>
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="hooker" value"1" class="form-check-input" id="hooker">
					                   		<label for="hooker">Hooker</label>
					                   </li>
 										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="jumper" value"1" class="form-check-input" id="jumper">
					                   		<label for="jumper">Lineout Jumper</label>
					                   </li>
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="lifter" value"1" class="form-check-input" id="lifter">
					                   		<label for="lifter">Lineout Lifter</label>
					                   </li>
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="kicker" value"1" class="form-check-input" id="kicker">
					                   		<label for="kicker">Lineout Kicker</label>
					                   </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Password:</label>
							<input type="password" name="password" class="form-control" placeholder="Choose your password" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Confrim Password:</label>
							<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm your password" required>
						</div>
					</div>
				</div>
				<button typ="submit" class="btn-secondary">Register <i class="fas fa-long-arrow-alt-right"></i></button>
			</form>
		</div>
	</div>
</div>
