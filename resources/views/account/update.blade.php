<div class="green-header"></div>
<div class="container py-3 my-5">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<h3>Update My Details</h3>
			<form action="" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Name:</label>
							<input type="name" name="name" class="form-control" value="{{ Auth::user()->name }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Email:</label>
							<input type="email" name="email" class="form-control" value="{{ Auth::user()->email }}" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Club Name:</label>
							<input type="text" name="club" class="form-control" value="{{ (Auth::user()->club!=='No Club') ? Auth::user()->club : '' }}">
						</div>
					</div>
					<div class="col-md-2">
						<ul class="phq-checkboxes pt-checkboxes mb-2">
							<li class="d-inline mb-3 pb-5">
								<input type="checkbox" name="club" value="No Club" id="club" {{ (Auth::user()->club=='No Club') ? 'checked' : '' }}><label for="club">No club</label>
							</li>
						</ul>
 					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>What's your usual position?</label>
							<select name="position" class="form-control" required>
								<option value="" disabled selected>Please choose an option...</option>
								<option value="forwards"
								{{ (Auth::user()->position=='forwards') ? 'selected' : '' }}
								>Forwards</option>
								<option value="backs"
								{{ (Auth::user()->position=='backs') ? 'selected' : '' }}
								>Backs</option>
								<option value="all-round"
								{{ (Auth::user()->position=='all-round') ? 'selected' : '' }}
								>Put me anywhere!</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Can you play any of these specialist positions? (select all that apply)</label>
							<div class="row">
								<div class="col-12">
									<ul class="phq-checkboxes pt-checkboxes mb-2">
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="tighthead" value="1" class="form-check-input" id="tighthead" {{ (Auth::user()->tighthead==1) ? 'checked' : '' }}> 
											<label for="tighthead">Tighthead Prop</label>
										</li>
									
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="loosehead" value="1" class="form-check-input" id="loosehead" {{ (Auth::user()->loosehead==1) ? 'checked' : '' }}> 
											<label for="loosehead">Loosehead Prop</label>
										</li>
									
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="hooker" value="1" class="form-check-input" id="hooker" {{ (Auth::user()->hooker==1) ? 'checked' : '' }}> 
											<label for="hooker">Hooker</label
											</li>
									
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="jumper" value="1" class="form-check-input" id="jumper" {{ (Auth::user()->jumper==1) ? 'checked' : '' }}> 
											<label for="jumper">Lineout Jumper</label>
										</li>
									
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="lifter" value="1" class="form-check-input" id="lifter" {{ (Auth::user()->lifter==1) ? 'checked' : '' }}> 
											<label for="lifter">Lineout Lifter</label>
										</li>
									
										<li class="d-inline mb-3 pb-5">
											<input type="checkbox" name="kicker" value="1" class="form-check-input" id="kicker" {{ (Auth::user()->kicker==1) ? 'checked' : '' }}> 
											<label for="kicker">Kicker</label>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button type="submit" class="btn-secondary">Update <i class="fas fa-check ml-2"></i></button>
			</form>
		</div>
	</div>
</div>
