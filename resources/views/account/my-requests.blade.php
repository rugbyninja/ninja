<div class="green-header"></div>
<div class="container mt-4 py-3 h100">
	<div class="row">
		<div class="col-md-12">
			<div class="card-deck">
				@foreach(Match::playerRequests() AS $key => $request)
				<div class="card card-border">
				@include('account.partials.requests')
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-md-12">
					{{ Match::playerRequests()->links() }}
				</div>
			</div>
		</div>
	</div>
</div>