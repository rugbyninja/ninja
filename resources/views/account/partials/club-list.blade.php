<div class="card">
  <div class="card-body">
    <h5 class="card-title">{{ Club::id($club->club_id)->club }} vs {{ $club->opposition }}</h5>
    <p>{{ \Carbon\Carbon::parse($club->date)->format('d-m-Y') }} - {{ $club->time }}</p>
    <p>{{ strtoupper($club->postcode) }} : {{ number_format($club->distance,2).'mi' }}</p>
    <p class="card-text">
    	@if($club->tighthead==1)
    	<badge class="badge badge-success">Tighthead</badge>
    	@endif
    	@if($club->loosehead==1)
    	<badge class="badge badge-success">Loosehead</badge>
    	@endif
    	@if($club->hooker==1)
    	<badge class="badge badge-success">Hooker</badge>
    	@endif
    	@if($club->lifter==1)
    	<badge class="badge badge-success">Lineout Lifter</badge>
    	@endif
    	@if($club->jumper==1)
    	<badge class="badge badge-success">Linout Jumper</badge>
    	@endif
    	@if($club->kicker==1)
    	<badge class="badge badge-success">Kicker</badge>
    	@endif
    </p>
    @if(Match::playerCheck($club->club_id, $club->id))
    <button class="btn btn-block btn-success btn-sm">Already Requested</button>
    @else
    <form action="/account/request" method="post">
    	{{ csrf_field() }}
    	<input type="hidden" name="club_looking_id" value="{{ $club->id }}">
    	<input type="hidden" name="originator" value="Player">
    	<input type="hidden" name="club_id" value="{{ $club->club_id }}">
    	<button type="submit" class="btn btn-block btn-primary btn-sm">Request Club</button>
	</form>
	@endif
  </div>
</div>