<div class="card-body">
    <h5 class="card-title">{{ Club::id($request->club_id)->club }} vs {{ $request->opposition }}</h5>
    <p class="mb-0">{{ \Carbon\Carbon::parse($request->date)->format('d-m-Y') }} - {{ $request->time }}</p>
    
	@if($request->tighthead==1)
    <p class="card-text">
	   <badge class="badge badge-success">Tighthead</badge>
	</p>
    @endif
	@if($request->loosehead==1)
    <p class="card-text">
	   <badge class="badge badge-success">Loosehead</badge>
	</p>
    @endif
	@if($request->hooker==1)
    <p class="card-text">
	   <badge class="badge badge-success">Hooker</badge>
	</p>
    @endif
	@if($request->lifter==1)
    <p class="card-text">
	   <badge class="badge badge-success">Lineout Lifter</badge>
	</p>
    @endif
	@if($request->jumper==1)
    <p class="card-text">
	   <badge class="badge badge-success">Linout Jumper</badge>
	</p>
    @endif
	@if($request->kicker==1)
    <p class="card-text">
	   <badge class="badge badge-success">Kicker</badge>
	</p>
    @endif
</div>
<div class="card-footer response-footer">
    @if($request->originator=='Player' || $request->date < date('Y-m-d', strtotime('last sunday')))
    @if($request->status=='Pending')
        <div class="btn-block card-response-awaiting">Awaiting Response</div>
    @elseif($request->status=='Declined')
        <div class="btn-block card-response-declined">Declined</div>
    @elseif($request->status=='Cancelled')
        <div class="btn-block card-response-cancelled">Cancelled</div>
    @else
        <div class="btn-block card-response-accepted">Accepted</div>
    @endif

    @else
    @if($request->status=='Pending')
        <div class="btn btn-block btn-info btn-sm" data-toggle="modal" data-target="#exampleModal">Pending</div>
    @elseif($request->status=='Declined')
        <div class="btn btn-block btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">Declined</div>
    @elseif($request->status=='Cancelled')
        <div class="btn btn-block btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">Cancelled</div>
    @else
        <div class="btn btn-block btn-success btn-sm" data-toggle="modal" data-target="#exampleModal">Accepted</div>
    @endif
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/account/request" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $request->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <select name="status" class="form-control" required>
                            <option value="" selected disabled>Please choose an option</option>
                            @foreach(Match::getStatuses() AS $status)
                            <option value="{{ $status }}" {{ ($request->status==$status) ? 'selected' : '' }}>{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
</div>
