<div class="green-header"></div>

<div class="container py-3 mt-5 h100">
	<div class="row">
		<div class="col-12">
			<h3>Clubs Looking Near You</h3>	
		</div>
		@foreach(ClubsLooking::randomLooking() AS $key => $club)
		<div class="col-md-3">
			@include('account.partials.club-list')
		</div>
		@endforeach
	</div>
	<div class="row">
		<div class="col-md-4">
			@if(Auth::user()->available==1)
				<label>You are currently available</label>
				<form action="/user/availability" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="available" value="0">
					<button type="submit" class="btn btn-danger">Make me unavailable</button>
				</form>
			@else
				<label>You are currently unavailable</label>
				<form action="/user/availability" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="available" value="1">
					<button type="submit" class="btn btn-success">Make me available!</button>
				</form>
			@endif
		</div>
		<div class="col-md-8">
			<form action="" method="get">
				<div class="row">
					<div class="col-md-3">
						<select name="position" class="form-control" required>
							<option value="" selected disabled>position...</option>
							@foreach(User::positions() AS $key => $position)
							<option value="{{ $key }}" {{ ($key==Request()->position) ? 'selected' : '' }}>{{ $position }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<input type="text" name="postcode" class="form-control" value="{{ Request()->postcode }}" placeholder="postcode..." required>
					</div>
					<div class="col-md-3">
						<select name="distance" class="form-control" required>
							<option value="" selected disabled>distance...</option>
							@foreach(User::distance() AS $key => $distance)
							<option value="{{ $key }}" {{ ($key==Request()->distance) ? 'selected' : '' }}>{{ $distance }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<button type="submit" class="btn btn-block btn-primary">Search</button>
					</div>
				</div>
			</form>
			<div class="row py-3">
				@foreach(ClubsLooking::search() AS $key => $club)
				<div class="col-md-4">
				@include('account.partials.club-list')
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-md-12">
					{{ ClubsLooking::search()->links() }}
				</div>
			</div>
		</div>
	</div>
</div>