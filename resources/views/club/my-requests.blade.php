<div class="container py-3">
	<div class="row">
		<div class="col-md-12">
			<div class="row py-3">
				@foreach(Match::clubRequests() AS $key => $request)
				<div class="col-md-4">
				@include('club.partials.requests')
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-md-12">
					{{ Match::clubRequests()->links() }}
				</div>
			</div>
		</div>
	</div>
</div>