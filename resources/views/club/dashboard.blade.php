<div class="green-header"></div>

<div class="container pt-5 pb-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card-deck no-scale">
				<div class="card p-5 card-border">
					{!! ClubsLooking::updateForm() !!}
				</div>
			</div>
		</div>
	</div>
</div>
<div class="available-players">
	<div class="container py-5">
		<h3>Players Available Near You</h3>
		<div class="card-deck slider">
			@foreach(User::randomAvailable() AS $key => $player)
			@include('club.partials.player-list')
			@endforeach
		</div>
	</div>
</div>


<div class="container py-5 h100">
	<div class="row">
		<div class="col-md-12">
			<h3>Search for a player</h3>
			<form action="" method="get">
				<div class="row pb-3">
					<div class="col-md-3">
						<select name="position" class="form-control" required>
							<option value="" selected disabled>position...</option>
							@foreach(User::positions() AS $key => $position)
							<option value="{{ $key }}" {{ ($key==Request()->position) ? 'selected' : '' }}>{{ $position }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<input type="text" name="postcode" class="form-control" value="{{ Request()->postcode }}" placeholder="postcode..." required>
					</div>
					<div class="col-md-3">
						<select name="distance" class="form-control" required>
							<option value="" selected disabled>distance...</option>
							@foreach(User::distance() AS $key => $distance)
							<option value="{{ $key }}" {{ ($key==Request()->distance) ? 'selected' : '' }}>{{ $distance }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<button type="submit" class="btn-block btn-search">Search <i class="fas fa-long-arrow-alt-right"></i></button>
					</div>
				</div>
			</form>
			<div class="row py-3">
				@foreach(User::search() AS $key => $player)
				<div class="col-md-4">
				@include('club.partials.player-list')
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-md-12">
					{{ User::search()->links() }}
				</div>
			</div>
		</div>
	</div>
</div>