<div class="card">
  <div class="card-body">
    <h5 class="card-title">{{ Auth::guard('club')->user()->club }} vs {{ $request->opposition }}</h5>
    <p>{{ \Carbon\Carbon::parse($request->date)->format('d-m-Y') }} - {{ $request->time }}</p>
    <p class="card-text">
    	@if($request->tighthead==1)
    	<badge class="badge badge-success">Tighthead</badge>
    	@endif
    	@if($request->loosehead==1)
    	<badge class="badge badge-success">Loosehead</badge>
    	@endif
    	@if($request->hooker==1)
    	<badge class="badge badge-success">Hooker</badge>
    	@endif
    	@if($request->lifter==1)
    	<badge class="badge badge-success">Lineout Lifter</badge>
    	@endif
    	@if($request->jumper==1)
    	<badge class="badge badge-success">Linout Jumper</badge>
    	@endif
    	@if($request->kicker==1)
    	<badge class="badge badge-success">Kicker</badge>
    	@endif
    </p>
    @if($request->originator=='Club' || $request->date < date('Y-m-d', strtotime('last sunday')))
    @if($request->status=='Pending')
    <button class="btn btn-block btn-info btn-sm">Awaiting Response</button>
    @elseif($request->status=='Declined')
    <button class="btn btn-block btn-danger btn-sm">Declined</button>
    @elseif($request->status=='Cancelled')
    <button class="btn btn-block btn-danger btn-sm">Cancelled</button>
    @else
    <button class="btn btn-block btn-success btn-sm">Accepted</button>
    @endif

    @else
    @if($request->status=='Pending')
    <button class="btn btn-block btn-info btn-sm" data-toggle="modal" data-target="#exampleModal">Pending</button>
    @elseif($request->status=='Declined')
    <button class="btn btn-block btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">Declined</button>
    @elseif($request->status=='Cancelled')
    <button class="btn btn-block btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">Cancelled</button>
    @else
    <button class="btn btn-block btn-success btn-sm" data-toggle="modal" data-target="#exampleModal">Accepted</button>
    @endif

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/club-account/request" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $request->id }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="status" class="form-control" required>
                                <option value="" selected disabled>Please choose an option</option>
                                @foreach(Match::getStatuses() AS $status)
                                <option value="{{ $status }}" {{ ($request->status==$status) ? 'selected' : '' }}>{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
  </div>
</div>