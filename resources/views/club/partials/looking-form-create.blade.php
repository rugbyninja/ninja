<form action="/club-account/availability" method="post">
	{{ csrf_field() }}
	<h3 class="text-left">Next Match Information</h3>
	<div class="row">
		<div class="form-group col-md-12 col-lg-4 text-left">
			<p class="mb-0">What postions do you need this week?</p>
			<ul class="phq-checkboxes pt-checkboxes mb-2">
				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="forwards" value="1" class="form-check-input" id="forwards"> 
					<label for="forwards">Forwards</label>
				</li>

				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="backs" value="1" class="form-check-input" id="backs"> 
					<label for="backs">Backs</label>
				</li>
			</ul>
		</div>
		<div class="form-group col-md-12 col-lg-8 text-left">
			<label>Any Specialists?</label><br>
			<ul class="phq-checkboxes pt-checkboxes mb-2">
				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="tighthead" value="1" class="form-check-input" id="tighthead"> 
					<label for="tighthead">Tighthead Prop</label>
				</li>

				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="loosehead" value="1" class="form-check-input" id="loosehead"> 
					<label for="loosehead">Loosehead Prop</label>
				</li>

				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="hooker" value="1" class="form-check-input" id="hooker"> 
					<label for="hooker">Hooker</label
					</li>

				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="jumper" value="1" class="form-check-input" id="jumper"> 
					<label for="jumper">Lineout Jumper</label>
				</li>

				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="lifter" value="1" class="form-check-input" id="lifter"> 
					<label for="lifter">Lineout Lifter</label>
				</li>

				<li class="d-inline mb-3 pb-5">
					<input type="checkbox" name="kicker" value="1" class="form-check-input" id="kicker"> 
					<label for="kicker">Kicker</label>
				</li>
			</ul>
		</div>
	</div>
	<div class="row text-left">
		<div class="col-sm-12 col-md-4 col-lg-4">
			<div class="form-group">
				<label>Oppostion</label>
				<input type="text" name="opposition" class="form-control" required>
			</div>
		</div>
		<div class="col-sm-12 col-md-4 col-lg-2">
			<div class="form-group">
				<label> Players needed</label>
				<input type="number" name="players_needed" class="form-control" step="1" min="0" max="15" required>
			</div>
		</div>
		<div class="col-sm-12 col-md-4 col-lg-2">
			<div class="form-group">
				<label>Postcode of match</label>
				<input type="text" name="postcode" class="form-control" required>
			</div>
		</div>
		<div class="col-12 col-md-6 col-lg-2">
			<label>Date</label>
			<input type="date" name="date" min="{{ date('Y-m-d') }}" max="{{ date('Y-m-d', strtotime('+2 weeks')) }}" class="form-control" required>
		</div>
		<div class="col-12 col-md-6 col-lg-2">
			<label>Kick Off</label>
			<input type="time" name="time" class="form-control" required>
		</div>
	</div>
	

	<button type="submit" class="btn-secondary text-right float-right">Submit</button>
</form>