
<form action="/club-account/availability" method="post">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
				<h3 class="text-left">Next Match Information</h3>
		</div>
		<div class="col-6 float-right text-right">
			  	<a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fas fa-sliders-h mr-3"></i> Filter your match information</a>
		</div>
	</div>
	
	<div class="row">
		<div class="collapse multi-collapse col-12" id="multiCollapseExample1">
			<div class="row">
				<div class="form-group col-md-12 col-lg-4 text-left">
					<p class="mb-0">What postions do you need this week?</p>
					<ul class="phq-checkboxes pt-checkboxes mb-2">
						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="forwards" value="1" {{ ($latest->forwards==1) ? 'checked' : '' }}>
							<label for="forwards">Forwards</label>
						</li>

						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="backs" value="1" {{ ($latest->backs==1) ? 'checked' : '' }}> 
							<label for="backs">Backs</label>
						</li>
					</ul>
				</div>
				<div class="form-group col-md-12 col-lg-8 text-left">
					<label>Any Specialists?</label><br>
					<ul class="phq-checkboxes pt-checkboxes mb-2">
						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="tighthead" value="1" {{ ($latest->tighthead==1) ? 'checked' : '' }}>
							<label for="tighthead">Tighthead Prop</label>
						</li>

						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="loosehead" value="1" {{ ($latest->loosehead==1) ? 'checked' : '' }}>
							<label for="loosehead">Loosehead Prop</label>
						</li>

						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="hooker" value="1" {{ ($latest->hooker==1) ? 'checked' : '' }}>
							<label for="hooker">Hooker</label>
						</li>

						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="jumper" value="1" {{ ($latest->jumper==1) ? 'checked' : '' }}> 
							<label for="jumper">Lineout Jumper</label>
						</li>

						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="lifter" value="1" {{ ($latest->lifter==1) ? 'checked' : '' }}> 
							<label for="lifter">Lineout Lifter</label>
						</li>

						<li class="d-inline mb-3 pb-5">
							<input type="checkbox" name="kicker" value="1" {{ ($latest->kicker==1) ? 'checked' : '' }}> 
							<label for="kicker">Kicker</label>
						</li>
					</ul>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="form-group">
						<label>Oppostion</label>
						<input type="text" name="opposition" class="form-control" value="{{ $latest->opposition }}" required>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-2">
					<div class="form-group">
						<label> Players needed</label>
						<input type="number" name="players_needed" class="form-control" step="1" min="0" max="15" value="{{ $latest->players_needed }}" required>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-2">
					<div class="form-group">
						<label>Postcode of match</label>
						<input type="text" name="postcode" class="form-control" value="{{ $latest->postcode }}" required>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2">
					<label>Date</label>
					<input type="date" name="date" min="{{ \Carbon\Carbon::now() }}" max="{{ date('Y-m-d', strtotime('+2 weeks')) }}" class="form-control" value="{{ $latest->date }}" required>
				</div>
				<div class="col-12 col-md-6 col-lg-2">
					<label>Kick Off</label>
					<input type="time" name="time" class="form-control" value="{{ $latest->time }}" required>
				</div>
					<div class="col-12 text-center mt-3">
					<button type="submit" class="btn-secondary">Submit</button>			
				</div>
 
		 	</div>
 		</div>
	</div>
</form>