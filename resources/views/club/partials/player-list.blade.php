<div class="col-12 card-border">
    <div class="card-body">
        <h5 class="card-title">{!! ucwords(str_replace('-',' ',$player->position)) !!} - {{ number_format($player->distance,2).'mi' }}</h5>
        <p class="card-text">
            @if($player->tighthead==1)
            <badge class="badge badge-success">Tighthead</badge>
            @endif
            @if($player->loosehead==1)
            <badge class="badge badge-success">Loosehead</badge>
            @endif
            @if($player->hooker==1)
            <badge class="badge badge-success">Hooker</badge>
            @endif
            @if($player->lifter==1)
            <badge class="badge badge-success">Lineout Lifter</badge>
            @endif
            @if($player->jumper==1)
            <badge class="badge badge-success">Linout Jumper</badge>
            @endif
            @if($player->kicker==1)
            <badge class="badge badge-success">Kicker</badge>
            @endif
        </p>
    </div>
    <div class="card-footer player-footer">
        @if(ClubsLooking::latestClubRequest()==0)
        <button class="btn btn-block btn-danger btn-sm">Please update your next match information to be able to make requests</button>
        @else

        @if(Match::clubCheck($player->id))
        <button class="btn-inverse">Already Requested</button>
        @else
        <form action="/club-account/request" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="player_id" value="{{ $player->id }}">
            <input type="hidden" name="originator" value="Club">
            <input type="hidden" name="club_looking_id" value="{{ ClubsLooking::latestClubRequest() }}">
            <button type="submit" class="btn-secondary btn-block">Request Player</button>
        </form>
        @endif

        @endif
    </div>
</div>

