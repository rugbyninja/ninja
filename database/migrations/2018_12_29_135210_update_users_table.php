<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('club')->nullable();
            $table->string('position')->nullable();
            $table->boolean('tighthead')->default(0);
            $table->boolean('loosehead')->default(0);
            $table->boolean('hooker')->default(0);
            $table->boolean('jumper')->default(0);
            $table->boolean('lifter')->default(0);
            $table->boolean('kicker')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('club');
            $table->dropColumn('position');
            $table->dropColumn('tighthead');
            $table->dropColumn('loosehead');
            $table->dropColumn('hooker');
            $table->dropColumn('jumper');
            $table->dropColumn('lifter');
            $table->dropColumn('kicker');
        });
    }
}
