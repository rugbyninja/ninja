<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsLookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs_looking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->nullable();
            $table->string('opposition')->nullable();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->string('postcode')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('forwards')->default(0);
            $table->boolean('backs')->default(0);
            $table->boolean('tighthead')->default(0);
            $table->boolean('loosehead')->default(0);
            $table->boolean('hooker')->default(0);
            $table->boolean('jumper')->default(0);
            $table->boolean('lifter')->default(0);
            $table->boolean('kicker')->default(0);
            $table->integer('players_needed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs_looking');
    }
}
